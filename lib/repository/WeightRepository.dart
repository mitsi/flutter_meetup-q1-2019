import 'package:flutter_demo_health_meetup/dao/WeightDao.dart';
import 'package:flutter_demo_health_meetup/model/Weight.dart';

class WeightRepository {
  final WeightDao weightDao;

  WeightRepository(this.weightDao);

  Future getAllWeights() => weightDao.getWeights();

  Future addWeight(Weight weight) => weightDao.addWeight(weight);

  Future deleteWeightById(int id) => weightDao.deleteWeight(id);
}
