import 'package:flutter_demo_health_meetup/dao/HeightDao.dart';
import 'package:flutter_demo_health_meetup/model/Height.dart';

class HeightRepository {
  final HeightDao heightDao;

  HeightRepository(this.heightDao);

  Future getAllHeights() => heightDao.getHeights();

  Future updateHeight(Height height) {
    return heightDao.updateHeight(height);
  }
}
