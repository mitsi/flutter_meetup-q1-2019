class Height {
  int id;
  int value;

  Height({this.id, this.value});

  factory Height.fromDatabaseJson(Map<String, dynamic> data) => Height(
        id: data['id'],
        value: data['value'],
      );

  Map<String, dynamic> toDatabaseJson() => {
        "id": this.id,
        "value": this.value,
      };
}
