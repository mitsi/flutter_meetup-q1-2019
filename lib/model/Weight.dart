class Weight {
  int id;
  double value;
  DateTime date;

  Weight({this.id, this.value, this.date});

  factory Weight.fromDatabaseJson(Map<String, dynamic> data) => Weight(
        id: data['id'],
        value: data['value'],
        date: DateTime.parse(data['date']),
      );

  Map<String, dynamic> toDatabaseJson() => {
        "id": this.id,
        "value": this.value,
        "date": this.date.toIso8601String(),
      };
}
