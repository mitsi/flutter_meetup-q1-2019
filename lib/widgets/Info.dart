import 'package:flutter/material.dart';

class Info extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Indice de Masse Corporel"),
        ),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                          "L'indice de masse corporelle ou IMC (en anglais, body mass index ou BMI) est une grandeur qui permet d'estimer la corpulence d’une personne. Inventé au milieu du xixe siècle par Adolphe Quetelet, mathématicien belge et l'un des fondateurs de la statistique moderne, cet indice est appelé aussi l'indice de Quetelet.\nIl se calcule en fonction de la taille et de la masse corporelle. Il a été conçu, au départ, pour les adultes de 18 à 65 ans, mais de nouveaux diagrammes de croissance ont vu le jour au cours des dernières décennies pour les enfants de 0 à 18 ans. Dans les deux cas, il constitue une indication et intervient dans le calcul de l'indice de masse grasse (IMG).",
                      style: TextStyle(fontSize: 16), textAlign: TextAlign.justify,),
                    )),
                Image.asset("resources/assets/imc.png")
              ],
            ),
          ),
        ));
  }
}
