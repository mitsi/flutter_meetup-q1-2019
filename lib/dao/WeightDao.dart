import 'dart:async';

import 'package:flutter_demo_health_meetup/database/Database.dart';
import 'package:flutter_demo_health_meetup/model/Weight.dart';

class WeightDao {
  final dbProvider;

  WeightDao(this.dbProvider);

  Future<int> addWeight(Weight weight) async {
    final db = await dbProvider.database;
    var result = db.insert(WEIGHT_DATABASE, weight.toDatabaseJson());
    return result;
  }

  Future<List<Weight>> getWeights() async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result = await db.query(WEIGHT_DATABASE);

    List<Weight> weights = result.isNotEmpty
        ? result.map((item) => Weight.fromDatabaseJson(item)).toList()
        : [];
    return weights;
  }

  Future<int> deleteWeight(int id) async {
    final db = await dbProvider.database;
    var result =
        await db.delete(WEIGHT_DATABASE, where: 'id = ?', whereArgs: [id]);

    return result;
  }
}
