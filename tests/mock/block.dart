import 'package:flutter_demo_health_meetup/bloc/HeightBloc.dart';
import 'package:flutter_demo_health_meetup/bloc/WeightBloc.dart';
import 'package:mockito/mockito.dart';

class MockBlocHeight extends Mock implements HeightBloc {}

class MockBlocWeight extends Mock implements WeightBloc {}

