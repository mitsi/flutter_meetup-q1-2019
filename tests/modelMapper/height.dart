import 'package:flutter_demo_health_meetup/model/Height.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('can map object', () async {
    var result = Height.fromDatabaseJson({
      "id": 1,
      "value": 180,
    });

    expect(result.id, equals(1));
    expect(result.value, equals(180));
  });

  test('can map object', () async {
    var result = Height(id: 1, value: 180).toDatabaseJson();

    expect(result["id"], equals(1));
    expect(result["value"], equals(180));
  });
}
