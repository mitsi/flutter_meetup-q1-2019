import 'package:flutter_demo_health_meetup/dao/HeightDao.dart';
import 'package:flutter_demo_health_meetup/model/Height.dart';
import 'package:flutter_demo_health_meetup/repository/HeightRepository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

void main() {
  test('Can update', () async {
    var mock = MockHeightDao();

    await HeightRepository(mock).updateHeight(Height(id: 1, value: 180));

    var capturedArgument =
        verify((mock).updateHeight(captureAny)).captured.single as Height;
    expect(capturedArgument.id, equals(1));
    expect(capturedArgument.value, equals(180));
  });

  test('Can get', () async {
    var mock = MockHeightDao();

    await HeightRepository(mock).getAllHeights();

    verify((mock).getHeights()).called(1);
  });
}

class MockHeightDao extends Mock implements HeightDao {}
