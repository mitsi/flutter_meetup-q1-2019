import 'package:flutter_demo_health_meetup/dao/WeightDao.dart';
import 'package:flutter_demo_health_meetup/model/Weight.dart';
import 'package:flutter_demo_health_meetup/repository/WeightRepository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

void main() {
  test('Can delete', () async {
    var mock = MockWeightDao();

    await WeightRepository(mock).deleteWeightById(1);

    var capturedArgument =
        verify(mock.deleteWeight(captureAny)).captured.single as int;
    expect(capturedArgument, equals(1));
  });

  test('Can get', () async {
    var mock = MockWeightDao();

    await WeightRepository(mock).getAllWeights();

    verify((mock).getWeights()).called(1);
  });

  test('Can add', () async {
    var mock = MockWeightDao();

    await WeightRepository(mock)
        .addWeight(Weight(value: 18, date: DateTime(2020, 03, 03, 00, 00, 00)));

    var capturedWeight =
        verify((mock).addWeight(captureAny)).captured.single as Weight;
    expect(capturedWeight.value, equals(18));
    expect(capturedWeight.date.toIso8601String(),
        equals("2020-03-03T00:00:00.000"));
  });
}

class MockWeightDao extends Mock implements WeightDao {}
