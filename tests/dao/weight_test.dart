import 'package:flutter_demo_health_meetup/dao/WeightDao.dart';
import 'package:flutter_demo_health_meetup/model/Weight.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../mock/database.dart';

void main() {
  test('Can get', () async {
    var mock = MockDatabaseProvider();
    final mockDb = MockDatabaseFactory();
    final mockExecutor= MockDatabaseExecutor();

    when(mockExecutor.query(any)).thenAnswer((_) => Future.value([]));
    when(mockDb.openDatabase(any)).thenAnswer((_) => Future.value(mockExecutor));
    when(mock.database).thenAnswer((_) => Future.value(mockExecutor));


    await WeightDao(mock).getWeights();

    var database = await mock.database;

    var capturedArgument = verify((database).query(captureAny)).captured.first;
    expect(capturedArgument, equals("Weight"));
  });

  test('Can add', () async {
    var mock = MockDatabaseProvider();
    final mockDb = MockDatabaseFactory();
    final mockExecutor= MockDatabaseExecutor();

    when(mockExecutor.query(any)).thenAnswer((_) => Future.value([]));
    when(mockDb.openDatabase(any)).thenAnswer((_) => Future.value(mockExecutor));
    when(mock.database).thenAnswer((_) => Future.value(mockExecutor));

    await WeightDao(mock).addWeight(
        Weight(value: 80.0, date: DateTime(2020, 03, 03, 0, 0, 0, 0)));

    var database = await mock.database;

    var capturedArgument = verify((database).insert(captureAny, captureAny, conflictAlgorithm: captureAnyNamed("conflictAlgorithm"), nullColumnHack: captureAnyNamed("nullColumnHack"))).captured;
    expect(capturedArgument[0], equals("Weight"));
    expect(capturedArgument[1]["value"], equals(80.0));
    expect(capturedArgument[1]["date"], equals("2020-03-03T00:00:00.000"));
  });

  test('Can delete', () async {
    var mock = MockDatabaseProvider();
    final mockDb = MockDatabaseFactory();
    final mockExecutor= MockDatabaseExecutor();

    when(mockExecutor.query(any)).thenAnswer((_) => Future.value([]));
    when(mockDb.openDatabase(any)).thenAnswer((_) => Future.value(mockExecutor));
    when(mock.database).thenAnswer((_) => Future.value(mockExecutor));

    await WeightDao(mock).deleteWeight(1);

    var database = await mock.database;

    var requestParams = verify((database).delete(captureAny, whereArgs: captureAnyNamed("whereArgs"), where: captureAnyNamed("where"))).captured;
    expect(requestParams[0], equals("Weight"));
    expect(requestParams[1], equals([1]));
    expect(requestParams[2], equals("id = ?"));
  });

  test('Can get serialized data', () async {
    var mock = MockDatabaseProvider();
    final mockDb = MockDatabaseFactory();
    final mockExecutor = MockDatabaseExecutor();

    when(mockExecutor.query(any)).thenAnswer((_) => Future.value([{"id": 1, "value": 180.0, "date": "2020-03-03T00:00:00.000" }]));
    when(mockDb.openDatabase(any))
        .thenAnswer((_) => Future.value(mockExecutor));
    when(mock.database).thenAnswer((_) => Future.value(mockExecutor));

    var result = await WeightDao(mock).getWeights();

    expect(result.length, equals(1));
    expect(result[0].id, equals(1));
    expect(result[0].value, equals(180.0));
    expect(result[0].date, equals(DateTime(2020,03,03,00,00,00)));
  });
}
