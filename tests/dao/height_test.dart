import 'package:flutter_demo_health_meetup/dao/HeightDao.dart';
import 'package:flutter_demo_health_meetup/model/Height.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../mock/database.dart';

void main() {
  test('Can update', () async {
    var mock = MockDatabaseProvider();
    final mockDb = MockDatabaseFactory();
    final mockExecutor = MockDatabaseExecutor();

    when(mockExecutor.query(any)).thenAnswer((_) => Future.value([]));
    when(mockDb.openDatabase(any))
        .thenAnswer((_) => Future.value(mockExecutor));
    when(mock.database).thenAnswer((_) => Future.value(mockExecutor));

    var heightIdInDb = 1;
    await HeightDao(mock).updateHeight(Height(id: heightIdInDb, value: 180));

    var database = await mock.database;

    var capturedArgument = verify((database).update(captureAny, captureAny,
            where: captureAnyNamed("where"),
            whereArgs: captureAnyNamed("whereArgs"),
        conflictAlgorithm: captureAnyNamed("conflictAlgorithm")))
        .captured;

    expect(capturedArgument[0], equals("Height"));
    expect(capturedArgument[1]["id"], equals(heightIdInDb));
    expect(capturedArgument[1]["value"], equals(180));
    expect(capturedArgument[2], equals("id = ?"));
    expect(capturedArgument[3], equals([heightIdInDb]));
  });

  test('Can get', () async {
    var mock = MockDatabaseProvider();
    final mockDb = MockDatabaseFactory();
    final mockExecutor = MockDatabaseExecutor();

    when(mockExecutor.query(any)).thenAnswer((_) => Future.value([]));
    when(mockDb.openDatabase(any))
        .thenAnswer((_) => Future.value(mockExecutor));
    when(mock.database).thenAnswer((_) => Future.value(mockExecutor));

    await HeightDao(mock).getHeights();

    var database = await mock.database;

    var capturedTableName = verify((database).query(captureAny)).captured.first;
    expect(capturedTableName, equals("Height"));
  });

  test('Can get serialized data', () async {
    var mock = MockDatabaseProvider();
    final mockDb = MockDatabaseFactory();
    final mockExecutor = MockDatabaseExecutor();

    when(mockExecutor.query(any)).thenAnswer((_) => Future.value([{"id": 1, "value": 180 }]));
    when(mockDb.openDatabase(any))
        .thenAnswer((_) => Future.value(mockExecutor));
    when(mock.database).thenAnswer((_) => Future.value(mockExecutor));

    var result = await HeightDao(mock).getHeights();

    expect(result.length, equals(1));
    expect(result[0].id, equals(1));
    expect(result[0].value, equals(180));
  });
}
