import 'dart:async';

import 'package:flutter_demo_health_meetup/bloc/WeightBloc.dart';
import 'package:flutter_demo_health_meetup/model/Weight.dart';
import 'package:flutter_demo_health_meetup/repository/WeightRepository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

void main() {
  test('Can get added value by stream', () async {
    var mockWeightRepository = MockWeightRepository();
    var addedHeight = Weight(id: 1, value: 80.0);
    var emptyValues = List<Weight>();
    bool firstCall = true;
    when(mockWeightRepository.getAllWeights()).thenAnswer((_) {
      if (firstCall) {
        firstCall = false;
        return Future.value(emptyValues);
      }
      return Future.value([addedHeight]);
    });
    var weightBloc = WeightBloc(
        mockWeightRepository); //HeightRepository(HeightDao(DatabaseProvider.dbProvider))
    Stream<List<Weight>> stream = weightBloc.weights;

    weightBloc.addWeight(addedHeight);

    await expectLater(stream, emitsInOrder([emptyValues, [addedHeight]]));

    verify(mockWeightRepository.addWeight(captureAny)).called(1);
    verify(mockWeightRepository.getAllWeights()).called(2);
  });

  test('Can get values after delete', () async {
    var mockWeightRepository = MockWeightRepository();
    var remainingValues = Weight(id: 1, value: 80.0);
    var allWeightsBeforeDeletion = [
      remainingValues,
      Weight(id: 2, value: 81.0)
    ];
    bool firstCall = true;
    when(mockWeightRepository.getAllWeights()).thenAnswer((_) {
      if (firstCall) {
        firstCall = false;
        return Future.value(allWeightsBeforeDeletion);
      }
      return Future.value([remainingValues]);
    });
    var weightBloc = WeightBloc(
        mockWeightRepository); //HeightRepository(HeightDao(DatabaseProvider.dbProvider))
    Stream<List<Weight>> stream = weightBloc.weights;

    weightBloc.deleteWeightById(2);

    await expectLater(stream, emitsInOrder([allWeightsBeforeDeletion, [remainingValues]]));
    verify(mockWeightRepository.deleteWeightById(captureAny)).called(1);
    verify(mockWeightRepository.getAllWeights()).called(2);
  });

  test('Can get value when subscribing', () async {
    var mockWeightRepository = MockWeightRepository();
    var addedHeight = Weight(id: 1, value: 180);
    when(mockWeightRepository.getAllWeights())
        .thenAnswer((_) => Future.value([addedHeight]));
    var heightBloc = WeightBloc(
        mockWeightRepository); //HeightRepository(HeightDao(DatabaseProvider.dbProvider))
    Stream<List<Weight>> stream = heightBloc.weights;
    List<Weight> expected = [addedHeight];

    await expectLater(stream, emits(expected));
    verify(mockWeightRepository.getAllWeights()).called(1);
  });
}

class MockWeightRepository extends Mock implements WeightRepository {}
